function text = example_nonlinear_reach_ARCH20_prodDes()
% example_nonlinear_reach_ARCH20_prodDes - production-destruction benchmark
%                                          from the 2020 ARCH competition
%
% Syntax:  
%    example_nonlinear_reach_ARCH20_prodDes()
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% 
% Author:       Niklas Kochdumper
% Written:      17-May-2020
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------


% Parameter ---------------------------------------------------------------

params.tFinal = 100;

% initial set
R0_I = zonotope(interval([9.5;0.01;0.01],[10;0.01;0.01]));
R0_P = zonotope(interval([9.98;0.01;0.01;0.296],[9.98;0.01;0.01;0.304]));
R0_IP = zonotope(interval([9.7;0.01;0.01;0.298],[10;0.01;0.01;0.302]));


% Reachability Options ----------------------------------------------------

% general options
options.zonotopeOrder = 20;
options.taylorTerms = 10;

options.errorOrder = 5;
options.intermediateOrder = 10;

options.simplify = 'optimize';

% select algorithm
options.alg = 'lin';
options.tensorOrder = 3;


% System Dynamics ---------------------------------------------------------

sys_I = nonlinearSys(3,1,@prodDes,options);
sys_P = nonlinearSys(4,1,@prodDesParam,options);
sys_IP = sys_P;


% Reachability Analysis ---------------------------------------------------

% reachability analyis (case I)
params.R0 = R0_I;
options.timeStep = params.tFinal/1700;

tic;
[Rcont_I,R_I] = reach(sys_I,params,options);
tComp_I = toc;

R_I = R_I{end}{end}.set;

% reachability analyis (case P)
params.R0 = R0_P;
options.timeStep = params.tFinal/3000;

tic;
[Rcont_P,R_P] = reach(sys_P,params,options);
tComp_P = toc;

R_P = R_P{end}{end}.set;

% reachability analyis (case IP)
params.R0 = R0_IP;
options.timeStep = params.tFinal/3000;

tic;
[Rcont_IP,R_IP] = reach(sys_IP,params,options);
tComp_IP = toc;

R_IP = R_IP{end}{end}.set;



% Verification ------------------------------------------------------------

% verification (case I)
res1 = 1; res2 = 1;

int = interval(R_I);
infi = infimum(int);

if infi(1) < 0 || infi(2) < 0 || infi(3) < 0
    res1 = 0; 
end

Rtemp = [1 1 1]*R_I;
intSum = interval(Rtemp);

if ~in(intSum,10)
    res2 = 0; 
end

resI = res1 & res2;
volI = volume(int);

disp(' ');
disp('Case I ------------------')
disp(['Specifications satisfied: ',num2str(res1 & res2)]);
disp(['Computation time: ',num2str(tComp_I),'s']);
disp(['Volume: ',num2str(volI)]);
disp(' ');

% verification (case P)
res1 = 1; res2 = 1;

int = interval(R_P);
infi = infimum(int);

if infi(1) < 0 || infi(2) < 0 || infi(3) < 0
    res1 = 0; 
end

Rtemp = [1 1 1 0]*R_P;
intSum = interval(Rtemp);

if ~in(intSum,10)
    res2 = 0; 
end

resP = res1 & res2;
volP = volume(int);

disp('Case P ------------------')
disp(['Specifications satisfied: ',num2str(res1 & res2)]);
disp(['Computation time: ',num2str(tComp_P),'s']);
disp(['Volume: ',num2str(volP)]);
disp(' ');

% verification (case IP)
res1 = 1; res2 = 1;

int = interval(R_IP);
infi = infimum(int);

if infi(1) < 0 || infi(2) < 0 || infi(3) < 0
    res1 = 0; 
end

Rtemp = [1 1 1 0]*R_IP;
intSum = interval(Rtemp);

if ~in(intSum,10)
    res2 = 0; 
end

resIP = res1 & res2;
volIP = volume(int);

disp('Case I&P ----------------')
disp(['Specifications satisfied: ',num2str(res1 & res2)]);
disp(['Computation time: ',num2str(tComp_IP),'s']);
disp(['Volume: ',num2str(volIP)]);
disp(' ');



% Visualization -----------------------------------------------------------

figure; hold on

% visualization (case I)
t = 0;
timeStep = params.tFinal/length(Rcont_I);

for i = 1:length(Rcont_I)
    
    % compute interval for z over time
    int_t = interval(t,t+options.timeStep);
    int_z = interval(project(Rcont_I{i}{1},3));
    int = cartProd(int_t,int_z);
    
    % plot reachable set
    plotFilled(int,[1,2],'b','EdgeColor','b'); 
    
    t = t + timeStep;
end

% visualization (case IP)
t = 0;
timeStep = params.tFinal/length(Rcont_IP);

for i = 1:length(Rcont_IP)
    
    % compute interval for z over time
    int_t = interval(t,t+options.timeStep);
    int_z = interval(project(Rcont_IP{i}{1},3));
    int = cartProd(int_t,int_z);
    
    % plot reachable set
    plotFilled(int,[1,2],[0 .6 0],'EdgeColor',[0 .6 0]); 
    
    t = t + timeStep;
end

% visualization (case P)
t = 0;
timeStep = params.tFinal/length(Rcont_P);

for i = 1:length(Rcont_P)
    
    % compute interval for z over time
    int_t = interval(t,t+options.timeStep);
    int_z = interval(project(Rcont_P{i}{1},3));
    int = cartProd(int_t,int_z);
    
    % plot reachable set
    plotFilled(int,[1,2],'r','EdgeColor','r'); 
    
    t = t + timeStep;
end

xlabel('t')
ylabel('z')
xlim([0,100]);
ylim([0,11]);
box on;

% text stored in .csv table
text{1} = ['CORA,PRDE20,I,',num2str(resI),',',num2str(tComp_I),',',num2str(volI)];
text{2} = ['CORA,PRDE20,P,',num2str(resP),',',num2str(tComp_P),',',num2str(volP)];
text{3} = ['CORA,PRDE20,IP,',num2str(resIP),',',num2str(tComp_IP),',',num2str(volIP)];