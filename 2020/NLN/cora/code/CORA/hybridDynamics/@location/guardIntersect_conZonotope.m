function R = guardIntersect_conZonotope(obj,R,guard,options)
% guardIntersect_conZonotope - constrained zonotope based enclosure of 
%                              guard intersections
%
% Syntax:  
%    R = guardIntersect_conZonotope(obj,R,guard,options)
%
% Inputs:
%    obj - object of class location
%    R - list of intersections between the reachable set and the guard
%    guard - guard set (class: constrained hyperplane)
%    options - struct containing the algorithm settings
%
% Outputs:
%    R - set enclosing the guard intersection
%
% References: 
%   [1] M. Althoff et al. "Zonotope bundles for the efficient computation 
%       of reachable sets", 2011
% 
% Author:       Niklas Kochdumper
% Written:      19-December-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    % convert all relevant reachable sets to constraind zonotopes
    R = convertSets(R,options);

    % intersect the reachable sets with the guard set
    R_ = cell(length(R),1);
    counter = 1;
    
    for i = 1:length(R)
       R_{counter} = R{i} & guard; 
       counter = counter + 1;
    end
    
    R = R_(~cellfun('isempty',R_));

    % calculate orthogonal basis with the methods in Sec. V.A in [1]
    B = calcBasis(obj,R,guard,options);
    
    % loop over all calculated basis 
    Z = cell(length(B),1);
    
    for i = 1:length(B)
        
        int = [];
        
        % loop over all reachable sets
        for j = 1:length(R)
           
            % interval enclosure in the transformed space
            intTemp = interval(B{i}'*R{j});
            
            % unite all intervals
            int = int | intTemp;
        end
        
        % backtransformation to the original space
        Z{i} = B{i} * zonotope(int);
    end
    
    % construct the enclosing zonotope bundle object
    if length(Z) == 1
       R = Z{1}; 
    else
       R = zonoBundle(Z); 
    end 
end



% Auxiliary Functions -----------------------------------------------------

function R_ = convertSets(R,options)
% convert all reachable sets to constrained zonotopes
    
    N = sum(cellfun(@(x) numel(x),R));
    R_ = cell(N,1);
    counter = 1;

    % loop over all relevant reachable sets
    for i = 1:length(R)
        
        if ~iscell(R{i})            % no split sets
            
            if isa(R{i},'polyZonotope')
               R{i} = zonotope(R{i}); 
            end
            
            temp = reduce(R{i},options.reductionTechnique,options.guardOrder);
            R_{counter} = conZonotope(temp);
            counter = counter + 1;
            
        else                        % sets are split
            
            for j = 1:length(R{i})
                
                if isa(R{i}{j},'polyZonotope')
                   R{i}{j} = zonotope(R{i}{j}); 
                end
                
                temp = reduce(R{i}{j},options.reductionTechnique,options.guardOrder);
                R_{counter} = conZonotope(temp);
                counter = counter + 1;
            end
        end
    end
end

%------------- END OF CODE --------------