import os
import string
import re
from subprocess import Popen, PIPE

# examples organized with input/output file/string results for parsing runtime results
example=[]
example.append(['NAV','NAV2-BD2', 'NAV2-BD2', '--directions 2 --time-step 0.1 --time-horizon 10 --depth 30 -v x1,x2'])
example.append(['NAV','NAV3-BD3', 'NAV3-BD3', '--time-step 0.1 --time-horizon 10 --depth 25 -v x1,x2'])
example.append(['NAV','NAV4-BD4', 'NAV4-BD4', '--time-step 0.01 --time-horizon 10 --depth 15 -v x1,x2'])

# results in ./result
if not os.path.isdir('result'):
	os.mkdir('result')
	os.chdir('result')
	# populate the results.csv file	
with open("./result/results.csv","w") as f:
   for i in range(0,len(example)):			
	   string_cmd = 'docker run xspeed -m ./PCDB/' + example[i][0] + '/' + example[i][1] + '.xml -c ./PCDB/' + example[i][0] + '/' + example[i][2] + '.cfg ' + example[i][3]
	   print('\nrunning '+ example[i][2])
	   p = Popen(string_cmd, stdout=PIPE, stderr=PIPE, stdin=PIPE, shell=True)
	   output = str(p.communicate())
	   safety = "Model is SAFE"	
	   if output.find(safety)== -1:
		safety_res = 0
	   else: 
		safety_res = 1
	   time_str = "User Time  (in Seconds) ="
           r_idx = output.find(time_str)
	   r_idx_time_start = int(r_idx)+len(time_str)+1
	   r_idx_time_end = int(r_idx)+len(time_str)+8

	   if r_idx >= 0:
		output_time = output[r_idx_time_start:r_idx_time_end];
		# remove non-numeric characters (except .)
		output_time = re.sub("[^\d\.]", "", output_time)
		f.write('xspeed, '+example[i][0]+', '+ example[i][1]+ ', '+ str(safety_res)+', '+output_time+'\n')
	
os.chdir('..')	
print("results.csv file of XSpeed created successfully :)\n")
