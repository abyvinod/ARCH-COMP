function [Zred]=reduceMethF(Z)
% reduceMethF - reduces a zonotope to a parallelotope by finding dominant
% directions
%
% Syntax:  
%    [Zred,t]=reduceMethF(Z)
%
% Inputs:
%    Z - zonotope object
%
% Outputs:
%    Zred - reduced zonotope (of order 1)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: 

% Author:       Matthias Althoff
% Written:      08-February-2011
% Last update:  16-March-2019 (vnorm replaced, sort removed)
% Last revision:---

%------------- BEGIN CODE --------------



%get Z-matrix from zonotope Z
Zmatrix=get(Z,'Z');
dim=length(Zmatrix(:,1));

%extract generator matrix
G=Zmatrix(:,2:end);

while length(G(1,:))>dim

    %sort by length
    h=vecnorm(G);

    %pick smallest generator and remove it from G
    [~,ind] = min(h);
    gen = G(:,ind);
    G(:,ind) = [];

    %compute correlation
    genNorm = gen'/norm(gen);
    corr = [];
    for i=1:length(G(1,:))
        corr(i) = genNorm*G(:,i)/norm(G(:,i));
    end

    %add generator to most correlating generator
    [~,ind] = max(abs(corr));
    G(:,ind(end)) = G(:,ind) + sign(corr(ind))*gen;
end


P=G;

%Project Zonotope into new coordinate system
Ztrans=pinv(P)*Z;
Zinterval=interval(Ztrans);
Zred=P*zonotope(Zinterval);



%------------- END OF CODE --------------
