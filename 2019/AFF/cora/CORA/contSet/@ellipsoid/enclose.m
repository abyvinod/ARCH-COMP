function [E] = enclose(E1,E2)
% enclose - Generates a ellipsoid that encloses two ellipsoids of equal order
% and dimension
%
% Syntax:  
%    [E] = enclose(E1,E2)
%
% Inputs:
%    E1 - first ellipsoid object
%    E2 - second zonotope object
%
% Outputs:
%    E - ellipsoid, that encloses E1 and E2
%
% Example: 
%    E1=ellipsoid(eye(2));
%    E2=ellipsoid([1,0;0,3],[1;-1]);
%    E=enclose(E1,E2);
%    plot(E1);
%    hold on
%    plot(E2);
%    plot(E);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author: Matthias Althoff
% Written: 30-September-2006 
% Last update: 22-March-2007
% Last revision: ---

%------------- BEGIN CODE --------------
if nargin~=2 || ( ~isa(E1,'ellipsoid') || ~isa(E2,'ellipsoid'))
    error('Wrong type of arguments');
end
if length(E1.Q)~=length(E2.Q)
    error('Ellipsoids have to have same dimensions');
end
if E1.dim<length(E1.Q) && E2.dim<length(E2.Q)
    error('The convex hull of the two ellipsoids cannot be a null set');
end

q1 = E1.q;
q2 = E2.q;
Q1 = E1.Q;
Q2 = E2.Q;
%Taken from https://www2.isye.gatech.edu/~nemirovs/Lect_ModConvOpt.pdf,
%3.7.3.2
%For certain scalar ellipsoids (length(E1.Q)==1) the result seems off by
%.0000x (something like that), meaning that the resulting E does not
%completely include E1,E2; also, it appears that the algorithm used is
%quite slow for higher dimensional ellipsoids
[V1,D1] = eig(Q1);
[V2,D2] = eig(Q2);
A1 = V1*sqrt(D1);
A2 = V2*sqrt(D2);
n = length(Q1);
Y = sdpvar(n,n);%symmetric assumption is fine here
z = sdpvar(n,1);
lbda1 = sdpvar;
lbda2 = sdpvar;
t = sdpvar;

M1 = [eye(n),Y*q1-z,Y*A1;
     (Y*q1-z)',1-lbda1,zeros(1,n);
     A1'*Y,zeros(n,1),lbda1*eye(n)];
M2 = [eye(n),Y*q2-z,Y*A2;
     (Y*q2-z)',1-lbda2,zeros(1,n);
     A2'*Y,zeros(n,1),lbda2*eye(n)];
Cnts = [t<=geomean(Y),M1>=0,M2>=0,Y>=0];
optimize(Cnts,-t,sdpsettings('verbose',0));

Q = inv(value(Y)^2);
q = value(Y)\value(z);
E = ellipsoid(Q,q);
%------------- END OF CODE --------------